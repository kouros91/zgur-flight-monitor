SET @limit_altitude=8000;
SET @day_diff=1;



INSERT INTO `aerostuff`.`flights_villecresnes_stats` (
	`stats_date`
    ,
    `number_of_planes`
    ,
    `number_high`
    ,
    `number_low`
    ,
    `avg_altitude_high`
    ,
    `avg_altitude_low`
    ,
    `lowest_altitude`
    ,
    `seen_at_start`
    ,
    `seen_at_end`
    )
VALUES
(
	( SELECT DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) )
    ,
	( SELECT COUNT(*) as number_of_planes FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) )
    ,
	( SELECT COUNT(*) FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) AND altitude >= @limit_altitude )
    ,
	( SELECT COUNT(*) FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) AND altitude < @limit_altitude )
    ,
	( SELECT ROUND(AVG(altitude)) FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) AND altitude >= @limit_altitude )
    ,
	( SELECT ROUND(AVG(altitude)) FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) AND altitude < @limit_altitude )
    ,
	( SELECT ROUND(MIN(altitude)) FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) AND altitude < @limit_altitude )
    ,
	( SELECT DATE_FORMAT( MIN(FROM_UNIXTIME(LEFT(seen_at,10))),"%H:%i") FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) )
    ,
	( SELECT DATE_FORMAT( MAX(FROM_UNIXTIME(LEFT(seen_at,10))),"%H:%i") FROM aerostuff.flights_villecresnes WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) = DATE(DATE_ADD(now(),INTERVAL -@day_diff DAY)) )
);