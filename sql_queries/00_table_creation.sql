﻿/*  **
	* Table creation
	*
	* Date: 2016.11.22
	*
	* Date: 2016.11.23
	* Change: add "id", remove "hdg", remove "distance_from_sucy", rename table
** */


CREATE TABLE `flights_villecresnes` (
  `flightid` INT NOT NULL AUTO_INCREMENT,
  `flightno` varchar(20) DEFAULT NULL,
  `sqwk` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `altitude` varchar(20) DEFAULT NULL,
  `speed` varchar(20) DEFAULT NULL,
  `vert_rate` varchar(20) DEFAULT NULL,
  `track` varchar(20) DEFAULT NULL,
  `seen_at` bigint DEFAULT NULL,
  `distance_from_villecresnes` varchar(20) DEFAULT NULL,
  `pretty_date` varchar(50) as ( DATE(FROM_UNIXTIME(LEFT(seen_at,10))) ),
  `pretty_time` varchar(50) as ( TIME(FROM_UNIXTIME(LEFT(seen_at,10))) ),
  PRIMARY KEY (`flightid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `flights_villecresnes_stats` (
  `stats_id` INT NOT NULL AUTO_INCREMENT,
  `stats_date` varchar(50) DEFAULT NULL,
  `number_of_planes` varchar(20) DEFAULT NULL,
  `number_high` varchar(20) DEFAULT NULL,
  `number_low` varchar(20) DEFAULT NULL,
  `avg_altitude_high` varchar(20) DEFAULT NULL,
  `avg_altitude_low` varchar(20) DEFAULT NULL,
  `lowest_altitude` varchar(20) DEFAULT NULL,
  `seen_at_start` varchar(20) DEFAULT NULL,
  `seen_at_end` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`stats_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;